#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>      //multiple file descriptor
#include <stdlib.h>
#include <unistd.h>


int main(void){
    DIR *dir;
    struct dirent *myDirEnt;
    char dev[16]; //device ID
    char devPath[128]; //path to device
    char buf[256]; //data from the device
    char tmpData[6]; //temp c
    char path[]= "/sys/bus/w1/devices";
    ssize_t numRead;
    dir = opendir(path);
    if(dir!=NULL){
       while((myDirEnt = readdir(dir))){
           if(strstr(myDirEnt->d_name,"28-")!=NULL){
            //    printf("%d\n",*(strstr(myDirEnt->d_name,"28-")+2));
               strcpy(dev,myDirEnt->d_name);
            //    printf("device found: %s\n",dev);

           }
        
           
       }
        (void) closedir(dir);
    }
    else{
        perror("Not able to open device directory");
        return 1;
    }

    // create absolute path
    sprintf(devPath, "%s/%s/w1_slave", path, dev);

    int fd = open(devPath, O_RDONLY);
    if(fd==-1){
        perror("Could not open dev file.");
        return -1;
    }

    while((numRead=read(fd,buf,256))>0){

        strncpy(tmpData,strstr(buf,"t=")+2,5);
        float temp = strtof(tmpData,NULL);
        printf("%.1f",temp/1000);
        // example for strtof: 
        // char array[] = "365.25 7.0"; 
    
        // // Character end pointer 
        // char* pend; 
    
        // // f1 variable to store float value 
        // float f1 = strtof(array, &pend); 
    
        // // f2 varible to store float value 
        // float f2 = strtof(pend, NULL); 
  
    }
    close(fd);
    // return 0;
}

